# M N E Li Ad A

* Multiplicative
* Non Commutative
* Exponential
* Linear Logic
* with Additive case
* and Applicative pattern matching

# Features and technologies

* pi calculus, lambdas and concatenative stack works together as one
* deep inference structural proofs for type system
* signatures for kinds, classes, types and sorts
* linear logic, interaction nets, proof nets and solo calculus
* core syntax without any keywords, any natural language can be used
* competition of algorithms in additive and applicative fashion with match and winner
* can be used for mathematics, cryptography, smart contracts, proofs and applications
* monads, arrows, transformers, functors and profunctors can be used
* concurrent asynchronous processes, offers, sessions with parallel logic of pi calculus
* user defined binary operators is supported, with fixity direction and level
* specific rewriting logic for new syntax operators can be defined by user

