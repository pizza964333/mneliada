
class ConsC a b
instance ConsC StackT ChanT
instance ConsC StackT CodeT
instance ConsC StackT a => StackT (QuotT a)
instance ConsC StackT a => ConsC UnitT a

type family Reduce a where
  Reduce (ConsT a b) = StackT

data Term a where
  Unit :: Term UnitT
  Chan :: [String] -> Term ChanT
  Code :: [String] -> Term CodeT
  Quot :: QuotC a => Term a -> Terr (QuotT a)
  Cons :: ConsC a b => Term a -> Term b -> Terr (ConsT a b)
  Comm :: CommC a b => Term a -> Term b -> Terr (CommT a b)







