﻿{-# LANGUAGE MultiParamTypeClasses, GADTs, TypeFamilies, FlexibleInstances #-}

import Text.ParserCombinators.ReadP
import Data.Char

data ArciT  a b
data ArcoT  a b
data ArpiT  a b
data ArpoT  a b
data SendT  a b
data BraceT a
data ApplyT a b
data BindT  a b
data AbstrT a b
data SoloT  a b
data PushT  a b
data IndetT
data SigT
data ReplT a

class UnitC  a   where
class ArciC  a b where
class ArcoC  a b where
class ArpiC  a b where
class ArpoC  a b where
class IndetC a   where
class SendC  a b where
class BraceC a   where
class ChanC  a   where
class ApplyC a b where
class ScopeC a   where
class BindC  a b where
class AbstrC a b where
class SoloC  a b where
class BinopC a   where
class PushC  a b where
class SigC   a   where
class ReplC  a   where
class MultC  a b where
class PlusC  a b where
class ConjC  a b where
class DisjC  a b where
class LInvC  a   where

data MultT a b
data PlusT a b
data ConjT a b
data DisjT a b
data LInvT a
data ProcT

instance IndetC IndetT
instance SigC   IndetT
instance ArciC SigT SigT
instance ArcoC SigT SigT
instance ArpiC SigT SigT
instance ArpoC SigT SigT
instance BraceC SigT
instance ReplC SigT

type Terr a = Term (Reduce a)

data Term a where
  Mult  :: MultC  a b => Term a -> Term b -> Terr (MultT a b)
  Plus  :: PlusC  a b => Term a -> Term b -> Terr (PlusT a b)
  Conj  :: ConjC  a b => Term a -> Term b -> Terr (ConjT a b)
  Didj  :: DisjC  a b => Term a -> Term b -> Terr (DisjT a b)
  LInv  :: LInvC  a   => Term a -> Terr (LInvT a)
  Sig   :: SigC   a   => Term a -> Term SigT
  Unit  :: UnitC  a   => Term a
  Brace :: BraceC a   => Term a -> Terr (BraceT a)
  Arci  :: ArciC  a b => Term a -> Term b -> Terr (ArciT a b)
  Arco  :: ArcoC  a b => Term a -> Term b -> Terr (ArcoT a b)
  Arpi  :: ArpiC  a b => Term a -> Term b -> Terr (ArpiT a b)
  Arpo  :: ArpoC  a b => Term a -> Term b -> Terr (ArpoT a b)
  Repl  :: ReplC  a   => Term a -> Terr (ReplT a)
  Indet :: IndetC a   =>  [String]  -> Term a
  Chan  :: ChanC  a   =>  [String]  -> Term a
  Scope :: ScopeC a   => [[String]] -> Term a -> Term a
  Send  :: SendC  a b => { chanToPipe :: Term a, nameToSend :: Term b } -> Terr (SendT  a b)
  Apply :: ApplyC a b => { argument   :: Term a, function   :: Term b } -> Terr (ApplyT a b)
  Abstr :: AbstrC a b => { nameToBind :: Term a, scopeA     :: Term b } -> Terr (AbstrT a b)
  Bind  :: BindC  a b => { abstract   :: Term a, scopeB     :: Term b } -> Terr (BindT  a b)
  Solo  :: SoloC  a b => { subject    :: Term a, objects    :: Term b } -> Terr (SoloT  a b)
  Binop :: BinopC a   => Term a -> Term b -> Term c -> Terr (ApplyT a (ApplyT c b))
  Push  :: PushC  a b => { stack      :: Term a, element    :: Term b } -> Terr (PushT  a b)
  
instance Show (Term a) where
  show (Sig a) = show a
  show Unit = " 1"
  show (Brace a) = " (" ++ show a ++ " )"
  show (Arci a b) = show a ++ " ->" ++ show b
  show (Arco a b) = show a ++ " <-" ++ show b
  show (Arpi a b) = show a ++ " =>" ++ show b
  show (Arpo a b) = show a ++ " <=" ++ show b
  show (Indet [a]) = " " ++ a
  show (Repl a) = " !" ++ show a

type family Reduce a where
  Reduce (ArciT SigT SigT) = SigT
  Reduce (ArcoT SigT SigT) = SigT
  Reduce (ArpiT SigT SigT) = SigT
  Reduce (ArpoT SigT SigT) = SigT
  Reduce (BraceT SigT) = SigT
  Reduce (ReplT SigT) = SigT
  Reduce a = a
  



