test01 = "a => IO <- b"
test02 = "a => IO s StateT <- b"
test03 = "a => StateT{s,IO} <- b"
test04 = "a => IO StateT{s} <- b"
test05 = "d -> (a => s IO Swap StateT <- b) < - c"
test06 = "(a -> b) -> c -> d"

main :: IO ()
main = do
  let a = read "a -> ! (b -> c) => d" :: Term SigT
  b <- getContents
  mapM_ print $ identTool b
  