module Stack where

data U
data Q a
data H a

type a :. b
infixl 8 :.

type a :* b
infixl 7 :*

class IdC a
instance IdC (a :. Q a)
instance IdC a => (a :. H b)

class CatC a
instance CatC (a :. Q b :. Q c)
instance CatC a => (a :. H b)
instance CatC (a :. Q b) => (a :. H c :. Q b)

class TakeC a
instance TakeC (a :. Q b :. Q c)

class DupC a
instance DupC (a :. Q b)

class DropC a
instance DropC (a :. Q b)

class PlugC a
instance PlugC (a :. H (Q b))
instance PlugC a => (a :. Q b)

type Terr a = Term (R a)
type E a = R (EvalT a)
type J a = R (JailT a)


type family R a where
  R (IdT   (a :. Q b))        = a :. b
  R (IdT   (a :. H b))        = R (IdT a) :. H b
  R (CatT  (a :. Q b :. Q c)) = a :. Q (U :. a :.   b)
  R (TakeT (a :. Q b :. Q c)) = a :. Q (U :. b :. Q a)
  R (DupT  (a :. Q b))        = a :. (U :. Q b :. W :* U :. W :. Q b)
  R (DropT (a :. Q b))        = a :. H (Q b)
  R (PlugT (a :. H (Q a)))    = a :. Q b
  R (PlugT (a :. Q b))        = R (R (PlugT a) :. Q b :. P "Swap")
  R (a :. P "Id")             = R (IdT a)
  R (a :. P "Take")           = R (TakeT a)
  R (a :. P "Swap") = E (J a :. Q U :. P "Take" :. P "Take" :. P "Id") 
  
data Term a where
  Id   :: IdC   a => Term a -> Terr (IdT   a) 
  Cat  :: CatC  a => Term a -> Terr (CatT  a)
  Take :: TakeC a => Term a -> Terr (TakeT a)
  Dup  :: DupC  a => Term a -> Terr (DupT  a)
  Drop :: DropC a => Term a -> Terr (DropT a)
  Plug :: PlugC a => Term a -> Terr (PlugT a)
  Eval :: EvalC a => Term a -> Terr (EvalT a)
  Jail :: JailC a => Term a -> Terr (JailT a)



