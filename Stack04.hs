


data Term a where
  Hnt :: [Integer] -> Term HntT
  ROp :: [String] -> Term ROpT
  LOp :: [String] -> Term LOpT
  UQt :: QuoteC a => Term a -> Terr (QuoteT a)
  RQt :: QuoteC a => Term a -> Terr (QuoteT a)
  LQt :: QuoteC a => Term a -> Terr (QuoteT a)
  Cat :: CatC a b => Term a -> Term b -> Terr (CatT a b)
  
normR   (UQt a `Cat` UQt b `Cat` ROp ["take"])
 = Just (LOp ["unTake"] `Cat` UQt (b `Cat` (UQt a)))
 
normR   (UQt a `Cat` UQt b `Cat` ROp ["cat"])
 = Just (LOp ["unCat"] `Cat` UQt (a `Cat` hint `Cat` b))
 where
  hintMax = getMaxHintC a `hMax` getMaxHintC b
  hint = nextHint hintMax
  
normR   (UQt a `Cat` ROp ["id"])
 = Just (LOp ["unId"] `Cat` a)
 
normL   (LOp ["unTake"] `Cat` UQt (b `Cat` (UQt a)))
 = Just (UQt a `Cat` UQt b `Cat` ROp ["take"])
 
normL   (LOp ["unCat"] `Cat` UQt a)
 | isUnCat a = Just (UQt b `Cat` UQt c `Cat` ROp ["cat"])
 where
  hint = getMaxHintC a
  (b,c) = unpairHintC hint a 
 
normL   (LOp ["unId"] `Cat` UQt a)
 = Just (UQt (UQt a) `Cat` ROp ["id"])
 
normL   (LOp ["unId"] `Cat` ROp a)
 = Just (UQt (ROp a) `Cat` ROp ["id"])
 
 
 


