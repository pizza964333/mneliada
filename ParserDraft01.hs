identTool :: String -> [(Int,[String],Bool)]
identTool a = map f1 b
 where
  b = filter (\a -> length a > 0) $ lines a
  f1 a = let (b,c) = break (/=' ') a
          in (length b,f2 c,isBracketsBalanced c && not (isUnfinishedOperators c))
  f2 o@(' ':' ':_) = [o]
  f2 (a:' ':b) = [a] : f2 b
  f2 a = [a]
  
openBrackets  = "([{<"
closeBrackets = ")]}>"
operators = "+.&|!"

isBracketsBalanced a = d == 0
 where
  b = map (\c -> length $ filter (==c) a) openBrackets
  c = map (\c -> length $ filter (==c) a) closeBrackets
  d = foldr1 max $ map abs $ zipWith (-) b c
  
isUnfinishedOperators a = (head $ dropWhile isSpace $ reverse a) `elem` operators

  
indetP :: ReadP (Term SigT)
indetP = do
  a <- satisfy isLower
  b <- munch   isAlphaNum
  return $ Sig (Indet [a:b] :: Term IndetT)
  
replP :: ReadP (Term SigT)
replP = do
  char '!'
  skipSpaces
  a <- sigP9
  return $ Repl a
 
arciP :: ReadP (Term SigT)
arciP = do
  a <- sigP9
  skipSpaces
  f <- foldr1 (<++) $ map (\(a,b) -> string a >> return b)
         [("->",Arci),("<-",Arco),("=>",Arpi),("<=",Arpo)]
  skipSpaces
  b <- sigP0
  skipSpaces
  return $ f a b
  
sigP9 :: ReadP (Term SigT)
sigP9 = indetP <++ bracesP <++ replP

sigP0 :: ReadP (Term SigT)
sigP0 = arciP <++ sigP9

bracesP = do
  char '('
  skipSpaces
  a <- sigP0
  char ')'
  skipSpaces
  return $ Brace a
  
instance Read (Term SigT) where
  readsPrec _ = readP_to_S (skipSpaces >> sigP0)