
import Text.Parsec.String
import Text.Parsec.Char
import Text.Parsec
import System.Random
import System.IO.Unsafe
import Data.List


main = do
  a <- getContents
  let (Right b) = parse topP "stdin" a
  print "result"
  mapM_ print b
  
data CommentText = CommentText String deriving (Eq)

instance Show CommentText where
  show (CommentText a) = "\n" ++ a ++ "\n"
  
data Elem = Text String | Binop String | Unary String
          | Send [[Elem]] | Recv [[Elem]]
          | Paren [Elem] | Number Integer | Unparsed String
          | Symbol String
          | Name [String] | SName [String] | CName [String]
          | Indent [[Elem]] [Elem] Int Int
          | Spaces Int
          | LongComment CommentText
          | ShortComment CommentText
          | AppR [[Elem]] [[Elem]]
          | AppL [[Elem]] [[Elem]]
          | Virt [Elem]
          | VMrk String
          | VParen [[Elem]]
  deriving (Show,Eq)
  
topP :: Parser [[Elem]]
topP = do
  a <- elemsP eof
  let b = indents (Indent [] [] 0 0 : (a++ [Indent [] [] 0 0]))
  let c = proc01 $ proc02 b
  let d = proc04 c
  let e = snd (proc03 d)
  return $ e
  
randomPair b = do
  a <- randomRIO (99999,999999 :: Integer)  
  let c = show a
  return (VMrk (b ++ "_open_" ++ c), VMrk (b ++ "_close_" ++ c)) 
  
proc04 ((VMrk a : b) : c)
  | isAOpen a = proc04 $ mkVParen01 (drop 7 a) [] (b:c)
--  | otherwise = VMrk a `dc` proc04 (b:c)
  
proc04 ((VParen a : b) : c) = VParen (proc04 a) `dc` proc04 (b:c)

proc04 ((a:b):c) = a `dc` proc04 (b:c)

proc04 ([]:c) = [] : proc04 c

proc04 [] = []
  
mkVParen01 a e ((Indent b cd g h : k) : p) | not (null cs) && matchAC a c
  = [vparen (reverse ((Indent b d g h : k2) : e))] : p2
 where
  cs = filter isVMrkAClose cd
  c = head cs
  d = delete c cs
  (k2:p2) = proc04 (k:p)
  
mkVParen01 a e ((b:c):d)
  | matchAC a b = (vparen (reverse e) : c):d
  | otherwise = mkVParen01 a (rdc b e) (c:d)
  
mkVParen01 a e ([]:d) = mkVParen01 a ([]:e) d

mkVParen01 a b c = error (show (a,b,c))

vparen a = VParen $ filter (not.null) a

isAOpen a = and $ zipWith (==) a "a_open_"
isAClose a = and $ zipWith (==) a "a_close_"

isVMrkAClose (VMrk a) = isAClose a
isVMrkAClose _ = False

dc a (b:c) = ((a:b):c)
dc a [] = [[a]]

rdc a (b:c) = ((b ++ [a]):c)
rdc a [] = [[a]]

sndm :: (a -> b) -> (c,a) -> (c,b)
sndm f (a,b) = (a,f b)

matchAC a (VMrk b) = isAClose b && (drop 8 b == a)
matchAC _ _ = False
  
proc03 ((Unary "!" : a) : b)
 = (True,snd (proc03 ([AppR [[Unary " !"]] (snd $ proc03 [a])] : b)))
 
proc03 ((VParen a : b) : c) = (d || g, VParen e `dc` h)
 where
  (d,e) = proc03 a 
  (g,h) = proc03 (b:c)

proc03 ((AppR a b : c) : d) = ( e || g || i, AppR f h `dc` j)
 where
  (e,f) = proc03 a
  (g,h) = proc03 b
  (i,j) = proc03 (c:d)
  
proc03 ((a:b):c) = case proc03 (b:c) of
  (True,(d:e)) -> (True,snd (proc03 ((a:d):e)))
  (_,(_:e)) -> (False, (a:b) : e)
  
proc03 ([]:a) = (False,[] : snd (proc03 a))

proc03 [] = (False,[])

proc02 [] = []
proc02 ((Indent a b c d : e) : [VMrk f] : g)
 = proc02 ((Indent a (b ++ [VMrk f]) c d : e) : g) 
proc02 ((Indent c d e f : a):b) | isCommEnded a
 = proc02 ((Indent c (last a : d) e f : init a):b)
proc02 ([Indent [] [] a b]:(Indent k w c d : e):f)
 = proc02 ((Indent k w (a+c) d : e) : f)
proc02 (g : (Indent k w c d : e) : f) | isCommented g
 = proc02 ((Indent (g : k) w c d : e) : f)
proc02 (x:xs) = x : proc02 xs 

isCommented [Indent _ _ _ _, LongComment _] = True
isCommented _ = False

isCommEnded a = f (reverse a)
 where
  f (Spaces _ : _) = True
  f (LongComment _ : _) = True
  f (ShortComment _ : _) = True
  f _ = False
    
proc01 :: [[Elem]] -> [[Elem]]
proc01 [] = []
proc01 (x:xs) | p x && (not (null xs)) = proc01 $ proc02 c
              | otherwise = x : proc01 xs
 where
   p x = isBinop (last x)
   (a,b) = spanIndent01 xs
   (op,cl) = unsafePerformIO (randomPair "a")
   c = [x ++ [op]] ++ a ++ [[cl]] ++ b
   
spanIndent01 (a@(Indent _ _ _ n : _) : b) = (a:c,d)
 where
  f (Indent _ _ _ n2 : _) = n2 >= n
  f _ = False
  (c,d) = span f b

isBinop (Binop _) = True
isBinop _ = False   
     
indents :: [Elem] -> [[Elem]]
indents a = case c of
  [] -> [e]
  d  -> e : indents c
 where
  (b,c) = break isIndent (tail a)
  e = head a : b

isIndent (Indent _ _ _ _) = True
isIndent _ = False      
          
elemsP :: Show a => Parser a -> Parser [Elem]
elemsP p = many1 c
 where
  a =  longComment <|> shortComment
       <|> parenP <|> binopP <|> unaryP
       <|> textP <|> numberP <|> symbolP
       <|> nameP
       <|> sendP <|> recvP <|> indentP <|> spacesP
  b = do
    notFollowedBy p
    d <- anyChar
    c <- manyTill anyChar (lookAhead (a <||> p))
    return (Unparsed (d:c))
  c = a <|> b

a <||> b = (a >> return ()) <|> (b >> return ())
          
          
unars1 = "!?-"
binops1 = "+*|&.@/~^:"
binops2 = ["->","<-","<=","=>"]

unaryP :: Parser Elem
unaryP = a
 where
  a = do
    a <- oneOf unars1
    return (Unary [a])

binopP :: Parser Elem
binopP = a <|> c
 where
  a = do
    char '`'
    a <- many1 (noneOf "`")
    char '`'
    return (Binop a)
  b = do
    a <- oneOf binops1
    return (Binop [a])
  f :: String -> Parser Elem
  f a = do
    b <- string a
    return (Binop b)
  c = foldr (\a b -> try (f a) <|> b) b binops2    
  
parenP :: Parser Elem
parenP = do
  char '('
  a <- elemsP (char ')')
  char ')'
  return (Paren a)
      
textP :: Parser Elem
textP = do
  char '"'
  a <- many p
  char '"'
  return (Text a)
 where
  p = a <|> b
   where
    a = string "\\\"" >> return '\"'
    b = noneOf "\""
    
numberP :: Parser Elem
numberP = do
  a <- many1 digit
  return (Number (read a))
  
symbolP :: Parser Elem
symbolP = do
  a <- upper
  b <- many alphaNum
  return (Symbol (a:b))
  
nameP :: Parser Elem
nameP = do
  a <- lookAhead j
  case a of
    [_] -> camelCaseNameP
    _   -> do a <- j; return (cnv a)
 where
  cnv o@(x:xs) = if all (=='_') (map head xs) then SName xs2 else Name xs2
   where xs2 = x : map tail xs
  j =  do
   lookAhead lower
   a <- many1 alphaNum
   b <- many (try f)
   return (a:b)
    where
     f = do
      b <- oneOf " _"
      a <- many1 alphaNum
      return (b : a)
  
camelCaseNameP :: Parser Elem
camelCaseNameP = do
  lookAhead lower
  a <- many1 (lower <|> digit)
  b <- many camelCaseP
  return (CName (a:b))
  
camelCaseP :: Parser String
camelCaseP = do
  a <- upper
  b <- many (lower <|> digit)
  return (a:b)
  
sendP :: Parser Elem
sendP = do
  char '{'
  notFollowedBy (char '#')
  a <- sepBy (elemsP (oneOf ",}")) (char ',')
  char '}'
  notFollowedBy (char '#')
  return (Send a)
  
recvP :: Parser Elem
recvP = do
  char '['
  a <- sepBy (elemsP (oneOf ",]")) (char ',')
  char ']'
  return (Recv a)
  
indentP :: Parser Elem
indentP = do
  endOfLine
  a <- many (notFollowedBy endOfLine >> space)
  return (Indent [] [] 1 (length a))
  
spacesP :: Parser Elem
spacesP = do
  a <- many1 (notFollowedBy endOfLine >> space)
  return (Spaces (length a)) 
  
commentText = a <|> b <|> c
 where
  a = do LongComment (CommentText b) <- longComment; return b
  b = do ShortComment (CommentText b) <- shortComment; return b
  c = do b <- noneOf "#"; return [b]
  
shortComment :: Parser Elem
shortComment = do
  char '#'
  a <- manyTill commentText (lookAhead endOfLine)
  return (ShortComment (CommentText ('#': concat a)))

longComment :: Parser Elem
longComment = do
  d <- a
  c <- manyTill commentText (lookAhead (try b))
  e <- b
  return (LongComment (CommentText (d ++ concat c ++ e)))
 where
  a = try (string "{#")
  b = string "}#"



 
  

 
  
  
