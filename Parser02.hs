
import Text.Parsec.String
import Text.Parsec.Char
import Text.Parsec
import System.Random
import System.IO.Unsafe
import Data.List
import Control.Monad
import Data.Functor.Identity
import Data.Dynamic
import Data.Maybe

syntaxTable =
 [("." ,[Short "Bind operator",InfixR 9])
 ,("*" ,[Short "Multiply",InfixL 7])
 ,("!" ,[Short "Replication operator",Unary])
 ,("&" ,[Short "Commutative case",InfixR 6])
 ,("@" ,[Short "Ordered case",InfixR 6])
 ,("~" ,[Short "Apply left to right"])
 ,(":" ,[Short "Scope separator",InfixL 10])
 ,("->",[Short "Pure channel input arrow"])
 ,("<-",[Short "Pure channel output arrow"])
 ,("=>",[Short "Process morphism input arrow"])
 ,("<=",[Short "Process morphism output arrow"])
 ,("+" ,[Short "Additive disjunction",InfixL 6])
 ,("|" ,[Short "Multiplicative disjunction"])
 ,("/" ,[Short "Division operator",InfixL 7,Prefix])
 ,("-" ,[Short "Negatation operator",InfixL 6,Prefix])
 ,("?" ,[Short "'Why not' exponential operator",Unary])
 ,("^" ,[Short "Algebraic power operator",InfixR 8])
 ]
 
isBinop :: Expr -> Bool
isBinop (Sntx a) = case lookup a syntaxTable of
 Just a  -> hasBinopProperty a
 Nothing -> False
isBinop _ = False

hasBinopProperty a = or $ map isBinopProperty a

isBinopProperty (InfixR _) = True
isBinopProperty (InfixL _) = True
isBinopProperty _ = False
 
syntaxP :: Parser Expr
syntaxP = b
 where
  a = do
    char '`'
    a <- many1 (noneOf "`")
    char '`'
    return (Sntx a)
  f :: String -> Parser Expr
  f a = do
    b <- string a
    return (Sntx b)
  b = foldr (\a b -> try (f a) <|> b) a (map fst syntaxTable)  
 
data Syntax = InfixL Double | InfixR Double | Short String
            | Prefix | Unary
 
type Text  =  String
type Words = [String]

data Expr
 = V Expr
 | Empt
 | Pren Expr
 | Sntx Text
 | NmeL Words
 | NmeS Words
 | NmeC Words
 | SmbL Words
 | SmbS Words
 | SmbC Words
 | Expr `AppR` Expr
 | Expr `AppL` Expr
 | CmnL Text
 | CmnS Text
 | Text Text
 | Numb Integer
 | Inde Int Expr
 | Spce Int
 | Code [([Expr],Expr,Expr)]
 | Send [Expr]
 | Recv [Expr]
 | Unpa Text
 | Dyna Dynamic

conv :: (Expr -> Expr) -> ([[Expr]] -> Expr, [[Expr]]) -> Expr
conv f (a,b) = f (a b)

isNotOnlyEmpty :: Expr -> Bool
isNotOnlyEmpty = stat . eCata j f
 where
  j _ = True
  f (a,b) | isComment      (a b) = dyna True
          | isNonEmptyLeaf (a b) = dyna False
  f (a,b) = dynFoldr1 (&&) b
          
isComment (CmnL _) = True
isComment (CmnS _) = True
isComment Empt = True
isComment _ = False

isNonEmptyLeaf (Sntx _) = True
isNonEmptyLeaf (NmeC _) = True
isNonEmptyLeaf (NmeS _) = True
isNonEmptyLeaf (NmeC _) = True
isNonEmptyLeaf (SmbL _) = True
isNonEmptyLeaf (SmbS _) = True
isNonEmptyLeaf (SmbC _) = True
isNonEmptyLeaf (Numb _) = True
isNonEmptyLeaf (Send _) = True
isNonEmptyLeaf (Recv _) = True
isNonEmptyLeaf (Unpa _) = True
isNonEmptyLeaf _ = False
                   
dyna a = Dyna (toDyn a)
stat (Dyna a) = fromJust (fromDynamic a)

dynFoldr1 :: Typeable a => (a -> a -> a) -> [[Expr]] -> Expr
dynFoldr1 f = dyna . foldr1 f . map stat . concat

proc01 = eCata j (conv f)
 where
  j _ = True
  f o@(Code _) = fillIndents o
  f a = a
  
proc02 = eCata j (conv f)
 where
  j _ = True
  f (Code [([],a,Empt)]) = a
  f a = a
  
fillIndents :: Expr -> Expr
fillIndents = Code . map (\a -> ([],a,Empt)) . f . indents
 where
  f :: [Expr] -> [Expr]
  f [] = []
  f (Code [([],Inde n Empt,Empt)] : Code a : b)
    | not (null a || isIndent (mid $ head a))
      = Inde n (opt $ Code a) : f b
  f (a:b) = opt a : f b 
  -- opt (Code [([],a,Empt)]) = a
  opt a = a
  
indents :: Expr -> [Expr]
indents (Code []) = []
indents (Code a) | null b = Code [head c] : indents (Code (tail c))
                 | otherwise = Code b : indents (Code c)
 where
  (b,c) = break (isIndent.mid) a
  
isIndent (Inde _ _) = True
isIndent _ = False    

mid (_,a,_) = a
 
eMapM :: Monad m => (Expr -> Bool) -> (Expr -> m Expr) -> Expr -> m ([[Expr]] -> Expr, [[Expr]])
eMapM p f o@(V    a  ) | p o = do b <- f a          ; return (\[[b]]   -> V    b    , [[b]]  )
eMapM p f o@(Pren a  ) | p o = do b <- f a          ; return (\[[b]]   -> Pren b    , [[b]]  )
eMapM p f o@(AppR a b) | p o = do c <- f a; d <- f b; return (\[[c,d]] -> AppR c d  , [[c,d]])
eMapM p f o@(AppL a b) | p o = do c <- f a; d <- f b; return (\[[c,d]] -> AppL c d  , [[c,d]])
eMapM p f o@(Inde a b) | p o = do c <- f b          ; return (\[[c]]   -> Inde a c  , [[c]]  )
eMapM p f o@(Send a  ) | p o = do b <- mapM f a     ; return (\[b]   -> Send b    , [b]  )
eMapM p f o@(Recv a  ) | p o = do b <- mapM f a     ; return (\[b]   -> Recv b    , [b]  )
eMapM p f o@(Code a  ) | p o = do b <- j a          ; return (\b       -> Code (q b), w b    )
 where j = mapM $ \(a,b,c) -> do
         d <- mapM f a; e <- f b; g <- f c; return (d,e,g)
       w [] = [] 
       w ((a,b,c):d) = a : [b] : [c] : w d
       q [] = []
       q (a:[b]:[c]:d) = (a,b,c) : q d
eMapM _ _ a = return (\[[a]] -> a, [[a]])

eCataM :: Monad m => (Expr -> Bool) -> (([[Expr]] -> Expr, [[Expr]]) -> m Expr) -> Expr -> m Expr
eCataM p f a = f =<< eMapM p (eCataM p f) a

-- eCata :: (Expr -> Bool) -> (Expr -> Expr) -> Expr -> Expr
eCata p f a = runIdentity $ eCataM p (return.f) a
 
instance Show Expr where
  show (V (Pren a)) = "#(#" ++ show a ++ "#)#"
  show Empt = ""
  show (Pren a) = "(" ++ show a ++ ")"
  show (Sntx a) = {- "`" ++ -} a {- ++ "`" -}
  show (NmeL a) = {- "#NL#" ++ -} delim " " a
  show (NmeS a) = {- "#NS#" ++ -} delim "_" a
  show (NmeC a) = {- "#NC#" ++ -} delim ""  a
  show (SmbL a) = "#SL#" ++ delim " " a
  show (SmbS a) = "#SS#" ++ delim "_" a
  show (SmbC a) = "#SC#" ++ delim ""  a
  show (AppR a b) = "#<#" ++ show a ++ "#AppR#" ++ show b ++ "#>#"
  show (AppL a b) = "#<#" ++ show a ++ "#AppL#" ++ show b ++ "#>#"
  show (CmnL a) = a
  show (CmnS a) = a
  show (Text a) = show a
  show (Numb a) = show a
  show (Inde a b) = "\\n\n" ++ replicate (a+1) '_' ++ show b
  show (Spce a) = replicate a ' '
  show (Send a) = "{" ++ delim "," (map show a) ++ "}"
  show (Recv a) = "[" ++ delim "," (map show a) ++ "]"
  show (Unpa a) = "#Unpa#" ++ a ++ "#/#"
  show (Code a) = "(;" ++ concatMap f a ++ ";)"
   where
    f (a,b,c) = concatMap (j.show) a ++ (k$show b) ++ (w$show c)
    j "" = ""
    j a = "#H#" ++ a ++ "#/H#"
    k a = {- "#R#" ++ -} a
    w "" = ""
    w a = "#E#" ++ a ++ "#/E#"
  
delim a b = drop (length a) $ concatMap (a++) b

mapr f (Right a) = Right (f a)
mapr _ (Left a) = Left a

main = do
  a <- getContents
  let b = parse topP "stdin" a
  let f = proc02 . proc01
  let c = mapr f b
  print c
  
numberOfNewLines = pred . length . lines

a <||> b = (a >> return ()) <|> (b >> return ())

lctp = do CmnL b <- longCommentP; return b
actp = do b <- anyChar; return [b]
  
shortCommentTextTill end = self
 where
  frag = lctp <|> actp
  self = (end >> return []) <|> loop
  loop = do a <- frag
            case numberOfNewLines a of
              0 -> do b <- self; return (a++b)
              _ -> return a
  
shortCommentP :: Parser Expr
shortCommentP = do
  char '#'
  a <- shortCommentTextTill (lookAhead (char '#' <||> endOfLine))
  b <- (char '#' >> return "#") <|> (lookAhead endOfLine >> return "")
  return (CmnS ("#" ++ a ++ b))

longCommentP :: Parser Expr
longCommentP = do
  d <- a
  c <- manyTill (lctp <|> actp) (lookAhead (try b))
  e <- b
  return (CmnL (d ++ concat c ++ e))
 where
  a = try (string "{#")
  b = string "}#"
  
parenP :: Parser Expr
parenP = do
  char '('
  a <- codeP (char ')')
  char ')'
  return (Pren a) 
  
indentP :: Parser Expr
indentP = do
  endOfLine
  a <- many (notFollowedBy endOfLine >> space)
  return (Inde (length a) Empt)
  
spacesP :: Parser Expr
spacesP = do
  a <- many1 (notFollowedBy endOfLine >> space)
  return (Spce (length a))
  
sendP :: Parser Expr
sendP = do
  char '{'
  notFollowedBy (char '#')
  a <- sepBy (codeP (oneOf ",}")) (char ',')
  char '}'
  notFollowedBy (char '#')
  return (Send a)
  
recvP :: Parser Expr
recvP = do
  char '['
  a <- sepBy (codeP (oneOf ",]")) (char ',')
  char ']'
  return (Recv a)

nameP :: Parser Expr
nameP = do
  a <- lookAhead j
  case a of
    [_] -> camelCaseNameP
    _   -> do a <- j; return (cnv a)
 where
  cnv o@(x:xs) = if all (=='_') (map head xs) then NmeS a else NmeL a
   where a = x : map tail xs
  j =  do
   lookAhead lower
   a <- many1 alphaNum
   b <- many (try f)
   return (a:b)
    where
     f = do
      b <- oneOf " _"
      a <- many1 alphaNum
      return (b : a)  
  
symbolP :: Parser Expr
symbolP = do
  a <- lookAhead j
  case a of
    [_] -> camelCaseSymbolP
    _   -> do a <- j; return (cnv a)
 where
  cnv o@(x:xs) = if all (=='_') (map head xs) then SmbS a else SmbL a
   where a = x : map tail xs
  j =  do
   lookAhead upper
   a <- many1 alphaNum
   b <- many (try f)
   return (a:b)
    where
     f = do
      b <- oneOf " _"
      a <- many1 alphaNum
      return (b : a)    
  
  
camelCaseNameP :: Parser Expr
camelCaseNameP = do
  lookAhead lower
  a <- many1 (lower <|> digit)
  b <- many camelCaseP
  return (NmeC (a:b))
  
camelCaseSymbolP :: Parser Expr
camelCaseSymbolP = do
  a <- many1 camelCaseP
  return (SmbC a)
  
camelCaseP :: Parser String
camelCaseP = do
  a <- upper
  b <- many (lower <|> digit)
  return (a:b)
  
textP :: Parser Expr
textP = do
  char '"'
  a <- many p
  char '"'
  return (Text a)
 where
  p = a <|> b
   where
    a = string "\\\"" >> return '\"'
    b = noneOf "\""
    
numberP :: Parser Expr
numberP = do
  a <- many1 digit
  return (Numb (read a)) 
     
codeP :: Show a => Parser a -> Parser Expr
codeP p = do d <- many1 c
             case d of
               -- [d] -> return d
               _   -> return (Code (map (\a -> ([],a,Empt)) d))
 where
  a =  longCommentP <|> shortCommentP
       <|> parenP <|> syntaxP
       <|> textP <|> numberP <|> symbolP
       <|> nameP
       <|> sendP <|> recvP <|> indentP <|> spacesP
  b = do
    notFollowedBy p
    d <- anyChar
    c <- manyTill anyChar (lookAhead (a <||> p))
    return (Unpa (d:c))
  c = a <|> b
  
  
topP :: Parser Expr
topP = do
  a <- codeP eof
  {-
  let b = indents (Indent [] [] 0 0 : (a++ [Indent [] [] 0 0]))
  let c = proc01 $ proc02 b
  let d = proc04 c
  let e = snd (proc03 d)
  -}
  return $ a




