module Stack02 where

data Term a where
  Chan :: ChanC a   => Term a -> [String] -> Term (ChanT a  )
  Send :: SendC a   => Term a             -> Terr (SendT a  )
  Recv :: RecvC a   => Term a             -> Terr (RecvT a  )
  Auto :: AutoC a   => Term a             -> Terr (AutoT a  )
  Hide :: HideC a b => Term a ->  Term b  -> Terr (HideT a b)
  Comm :: CommC a b => Term a ->  Term b  -> Terr (CommT a b)
  

  
{-

! [q,c] . cat{q} . q[a,b] * {c}
  . ! b[d] . c{d}
  .   b[d] . d[Markered] * c{d}
  . ! a[d] . c{d}
  
! [q] . unCat{q} . q[a] * {b,c}
  . ! a[d] . c{d}
  .   a[d] . d{Markered} * c{d}
  . ! a[d] . b{d}  
 
! [q,p,w] . arr{q} . q [a] * {p} * p{w} * w{a}

! [q,w] . drop{q} * q[a]{w} . a[b] * w[c] . b{PushV,c}

! [q,w] . plug{q} * q[a]{w} . a[b] . b{PopV}[c] . w{c}

! [q,w,r]  . take{q} * q[a]{w} * w[c,b] * w{r}
  . r{b} * a{PushU,unTake}
  . ! r[d] . c{d}
  . ! c[d] . r{d}
  
! [q,w] . unTake{q} * q{w} * w[a] * w{take,c}
  . ! c[d] . a{d}
  . ! a[d] . c{d}
  .   a[d] . w{d}
  
! [q,w,r,p,h] . cat{q} * q{w} * w[a,b] * w{p}
  . ! p[c] . a{c}
  . * . ! a[c] . r{c}
      . ! p[c] . r{c}
    * . ! r[c] . b{c}
      . ! b[c] . h{c}
      . ! r[c] . h{c}
  
-}